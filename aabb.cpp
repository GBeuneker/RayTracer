#include "template.h"
#include "AABB.h"

AABB::AABB() {}

AABB::AABB(vec3 _min, vec3 _max)
{
    min = _min;
    max = _max;
}

// Returns the surface area of the Axis-Aligned Bounding Box.
float AABB::GetSurfaceArea()
{
    float xLength = max.x - min.x;
    float yLength = max.y - min.y;
    float zLength = max.z - min.z;

    return xLength * yLength * 2 + xLength * zLength * 2 + yLength * zLength * 2;
}

// Check if a ray intersects the Bounding Box.
bool AABB::Intersection(Ray _ray)
{
    //source: https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
    vec3 rayOr = _ray.GetOrigin();
    vec3 rayDir = _ray.GetDirection();
    float t;

    float tmin = (min.x - rayOr.x) / rayDir.x;
    float tmax = (max.x - rayOr.x) / rayDir.x;
    if (tmin > tmax) swap(tmin, tmax);

    float tymin = (min.y - rayOr.y) / rayDir.y;
    float tymax = (max.y - rayOr.y) / rayDir.y;
    if (tymin > tymax) swap(tymin, tymax);

    if ((tmin > tymax) || (tymin > tmax))
        return false;

    if (tymin > tmin)
        tmin = tymin;

    if (tymax < tmax)
        tmax = tymax;

    float tzmin = (min.z - rayOr.z) / rayDir.z;
    float tzmax = (max.z - rayOr.z) / rayDir.z;

    if (tzmin > tzmax) swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
        return false;

    if (tzmin > tmin)
        tmin = tzmin;

    if (tzmax < tmax)
        tmax = tzmax;

    t = tmin;
    if (t < 0)
    {
        t = tmax;
        if (t < 0) return false;
    }
    return true;
}