#include "template.h"
#include "screen.h"


Screen::Screen(float _width, float _height)
{
	width = _width;
	height = _height;
	ratio = _width / _height;
}

/// <summary>Set all the calculated corners and screen center. </summary>
void Screen::Initialize(vec3 _center, vec3 _p0, vec3 _p1, vec3 _p2)
{
	center = _center;
	p0 = _p0;
	p1 = _p1;
	p2 = _p2;
}
