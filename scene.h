#pragma once
#include "primitives.h"
#include "bvh.h"

extern vector<Primitive*> primitives;
extern uint* primIndices;

class Scene
{
public:
    Scene();
    ~Scene();
    void Initialize();
    int GetLightAmount() { return lights.size(); }
    Light* GetLight(int _index) { return lights.at(_index); }
    Light* GetRandomLight();
    BVH* GetBVH() { return bvhTree; }
    void AddModel(vector<Triangle*> _model);
private:
    void BuildPrims();
    void BuildBVH();
    vector<Light*> lights;
    BVH* bvhTree;
};


