#pragma once
#include "template.h"
#include "screen.h"
#include "ray.h"
#include "game.h"

class Camera
{
public:
	Camera(vec3 _position, vec3 _dir, float _fov, Screen _screen, Surface* _surface); //constructor
	vec3 GetPosition() { return position; }
	vec3 GetDirection() { return direction; }
	Screen GetScreen() { return screen; }
	float GetFOV() { return fov; }
	void Move(vec3 _mov);
	void Rotate(float xRot, float yRot);
private:
	vec3 position;
	vec3 direction;
	float fov;
	Screen screen;
	Surface* surface;
	void UpdateScreen();
};