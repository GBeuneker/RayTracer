#pragma once
#include "AABB.h"
#include "primitives.h"
#include "bvhNode.h"

extern int traversals;
extern AABB CalculateBounds(int first, int count, uint* _primIndices);
extern AABB CalculateBounds(BVHNode* _node);
extern BVHNode** bvhPool;
extern uint poolIndex;

class BVH
{
public:
	BVH();
	void ConstructBVH();
	BVHNode* GetRoot() { return root; }
private:
	BVHNode* root;
};