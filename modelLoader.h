#pragma once

#include "primitives.h"
#include "scene.h"

class ModelLoader
{
public:
	//Loads a model into the scene
	static void LoadModel(string name, Scene* scene, vec3 position, float modelScale);
};