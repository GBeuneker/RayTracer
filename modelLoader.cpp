#include "template.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include "modelLoader.h"

// The model loader loads an obj file, and optionally a diffuse texture by looking at the first material's diffuse texture property (this must exist if 'loadModelTexture' is true)
void ModelLoader::LoadModel(string name, Scene* scene, vec3 position, float modelScale)
{
	vector<Triangle*> loadedTriangles = vector<Triangle*>();

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	string basePath = "models/";
	string objPath = basePath + name + ".obj";

	bool loadSuccess = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, objPath.c_str(), basePath.c_str());
	if (!loadSuccess)
	{
		cout << "Load model error: " << err.c_str() << endl;
		return;
	}

	for (tinyobj::shape_t shape : shapes)
	{
		unsigned int triangleCount = shape.mesh.num_face_vertices.size();

		for (unsigned int i = 0; i < triangleCount * 3; i += 3)
		{
			vec3 vert1 = vec3(attrib.vertices[3 * shape.mesh.indices[i].vertex_index],
				attrib.vertices[3 * shape.mesh.indices[i].vertex_index + 1],
				attrib.vertices[3 * shape.mesh.indices[i].vertex_index + 2]);

			vec3 vert2 = vec3(attrib.vertices[3 * shape.mesh.indices[i + 1].vertex_index],
				attrib.vertices[3 * shape.mesh.indices[i + 1].vertex_index + 1],
				attrib.vertices[3 * shape.mesh.indices[i + 1].vertex_index + 2]);

			vec3 vert3 = vec3(attrib.vertices[3 * shape.mesh.indices[i + 2].vertex_index],
				attrib.vertices[3 * shape.mesh.indices[i + 2].vertex_index + 1],
				attrib.vertices[3 * shape.mesh.indices[i + 2].vertex_index + 2]);

			Triangle* t = new Triangle(vert1*modelScale + position, vert2*modelScale + position, vert3*modelScale + position);
			//Triangle* flipped = new Triangle(vert1*modelScale + position, vert3*modelScale + position, vert2*modelScale + position);

			t->GetMaterial()->SetDiffuse(1);
			t->GetMaterial()->SetColor(Color(0, 1, 0));
			//flipped->GetMaterial()->SetDiffuse(1);
			//flipped->GetMaterial()->SetColor(Color(0, 1, 0));

			loadedTriangles.push_back(t);
			//loadedTriangles.push_back(flipped);
		}
	}

	scene->AddModel(loadedTriangles);
}