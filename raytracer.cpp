#include "template.h"
#include "raytracer.h"
#include "ray.h"
#include "bvh.h"
#include "scene.h"

/// <summary> The object that performs the raytracing operations. </summary>
/// <param name="_camera"> The camera we want to render from. </param>
/// <param name="_scene"> The scene we want to render.</param>
/// <param name="_screen"> The screen we want to plot pixels to. </param>
Raytracer::Raytracer(Scene* _scene)
{
    scene = _scene;
    shadowRay = Ray(vec3(0, 0, 0), vec3(0, 0, 1));
}

#pragma region Ray Casting

Color Raytracer::SampleDirect(Ray _hitRay, Primitive* _prim)
{
    vec3 rayPosition = _hitRay.GetPosition();

    // Get a random light in the scene and get a random point/normal on that light
    Light* randomLight = scene->GetRandomLight();
    vec3 randomLightPos = randomLight->GetRandomPoint();
    vec3 randomLightNormal = randomLight->GetNormal(randomLightPos);

    // Vector towards random point on light
    vec3 L = randomLightPos - rayPosition;
    float dist = LENGTH(L);
    L /= dist;
    float cos_o = DOT(-L, randomLightNormal);
    float cos_i = DOT(L, _hitRay.GetNormal());

    if (_prim->GetMaterial()->IsLight())
        return _prim->GetMaterial()->GetColor();

    // Break if light is behind surface point
    if (cos_o <= 0 || cos_i <= 0) return Color(0, 0, 0);

    Ray r = Ray(rayPosition + L * EPSILON, L);
    if (r.GetHitState() == Ray::HIT && _prim->GetType() != Primitive::LIGHT)
        return Color(0, 0, 0);

    // Check if we can reach a light
    Ray lightRay = Ray(_hitRay.GetPosition() + L * EPSILON, L);
    if (Intersect(lightRay) != randomLight)
        return Color(0, 0, 0);

    float BRDF = _prim->GetMaterial()->GetDiffuse() * INVPI;
    float solidAngle = (cos_o * randomLight->GetArea()) / (dist*dist);
    Color finalColor = randomLight->GetColor();
    finalColor *= BRDF * scene->GetLightAmount() * randomLight->GetIntensity() * solidAngle * cos_i;

    return finalColor;
}

Color Raytracer::SampleIndirect(Ray _hitRay, Primitive* _prim, bool lastSpecular)
{
    if (_hitRay.GetHitState() == Ray::NOHIT)
        return Color(0, 0, 0);

    // Next Event Estimation
    if (_prim->GetMaterial()->IsLight())
    {
        if (lastSpecular)
            return _prim->GetMaterial()->GetEmittence();
        else
            return Color(0, 0, 0);
    }

    // Russian roulette
    float surviveProb = clamp(MAX(MAX(_prim->GetMaterial()->GetColor().r, _prim->GetMaterial()->GetColor().g), _prim->GetMaterial()->GetColor().b), 0.0f, 1.0f);
    if (Xor32RNG(seed) > surviveProb)
        return Color(0, 0, 0);

    Light* randomLight = scene->GetRandomLight();
    vec3 lightPos = randomLight->GetRandomPoint();
    vec3 L = lightPos - _hitRay.GetPosition();
    vec3 Nl = randomLight->GetNormal(lightPos);
    float dist2 = SQRLENGTH(L);
    float A = randomLight->GetArea();
    NORMALIZE(L);
    Color Ld = Color(0, 0, 0);

    Ray lightRay = Ray(_hitRay.GetPosition() + L * EPSILON, L);
    if (DOT(_hitRay.GetNormal(), L) > 0 && DOT(Nl, -L) > 0)
    {
        // If we can reach our light
        if (Intersect(lightRay) == randomLight)
        {
            float solidAngle = (DOT(Nl, -L) * A) / dist2;
            Ld = randomLight->GetColor() * solidAngle * DOT(_hitRay.GetNormal(), L);
        }
    }

    // Probability to go specular
    float diffuseProb = clamp(_prim->GetMaterial()->GetDiffuse(), 0.0f, 1.0f);
    bool specular = Xor32RNG(seed) > diffuseProb;
    // We hit a specular surface
    if (specular)
    {
        Ray newRay = Reflect(_hitRay.GetDirection(), _hitRay.GetPosition(), _hitRay.GetNormal());
        if (DOT(_hitRay.GetNormal(), newRay.GetDirection()) < EPSILON)
            return Color(0, 0, 0);

        // Get new primitive
        _prim = Intersect(newRay);
        Color indirect = SampleIndirect(newRay, _prim, true) * DOT(_hitRay.GetNormal(), newRay.GetDirection());

        return indirect / surviveProb;
    }
    // We hit a diffuse surface
    else
    {
        Color BRDF = _prim->GetMaterial()->GetColor() * INVPI;
        // Importance Sampling
        vec3 reflectDir = CosineWeightedDiffuseReflection(_hitRay.GetNormal());
        if (DOT(_hitRay.GetNormal(), reflectDir) < EPSILON)
            return Color(0, 0, 0);

        Ray newRay = Ray(_hitRay.GetPosition() + reflectDir * EPSILON, reflectDir);

        // Get new primitive
        _prim = Intersect(newRay);
        // Calculate new color
        float PDF = DOT(_hitRay.GetNormal(), reflectDir) / PI;
        Color indirect = SampleIndirect(newRay, _prim, false) * DOT(_hitRay.GetNormal(), reflectDir) / PDF;

        return BRDF * indirect / surviveProb + Ld * BRDF;
    }

}

#pragma endregion

#pragma region Ray Intersection

Primitive* Raytracer::Intersect(Ray &_ray)
{
    // Get the normal, position and primitive index in the nearest intersection point of a ray.
    int primIndex = -1;
    int planeIndex = -1;
    _ray.SetHitState(Ray::HIT);

    if (TRACEMETHOD == WHITTEDTRACE)
        NearestIntersection(_ray, primIndex);
    else if (TRACEMETHOD == BVHTRACE)
        scene->GetBVH()->GetRoot()->Traverse(_ray, primIndex, false);

    if (primIndex >= 0)
        return primitives[primIndex];
    else
    {
        _ray.SetHitState(Ray::NOHIT);
        return 0;
    }
}

#pragma endregion

#pragma region Ray Utility Functions

/// <summary> Determine the nearest intersection point along a ray. </summary>
/// <param name="_ray"> The ray we want to check. </param>
/// <param name="normal"> OUT: The normal of the intersection point.</param>
/// <param name="rayPosition"> OUT: The position of the intersection point. </param>
/// <param name="primIndex"> OUT: the index of the primitive that was hit. </param>
/// <returns> The distance we need to travel along the ray to hit a primitive. -1 if we didn't hit anything. </returns>
void Raytracer::NearestIntersection(Ray &_ray, int &primIndex_out)
{
    int closestPrimIndex = -1;
    float nearestIntersection = _ray.GetDistance();

    // Check all the primitives
    for (size_t i = 0; i < primitives.size(); i++)
    {
        // Get the closest intersection distance with the primitive.
        float intersectionDist = primitives[i]->GetIntersectionDist(_ray);

        // We have found a valid intersection which is nearer than our current nearest intersection
        if (intersectionDist >= 0 && intersectionDist < nearestIntersection)
        {
            // Save the new intersection distance
            nearestIntersection = intersectionDist;
            // Save the index of the primitive intersected
            closestPrimIndex = i;
        }
    }

    if (closestPrimIndex >= 0)
    {
        // Set the index of the primitive
        primIndex_out = closestPrimIndex;
        // Set the intersection distance and get the intersection point
        _ray.SetDistance(nearestIntersection);
        vec3 intersectionPoint = _ray.GetPosition();
        // Calculate normal
        vec3 n = primitives[closestPrimIndex]->GetNormal(intersectionPoint);
        NORMALIZE(n);
        _ray.SetNormal(n);
    }
}

Ray Raytracer::Reflect(vec3 _rayDirection, vec3 _hitPosition, vec3 _normal)
{
    vec3 reflectDirection = _rayDirection - 2 * (DOT(_rayDirection, _normal))*_normal;
    return Ray(_hitPosition + reflectDirection * 0.0005f, reflectDirection);
}

vec3 Raytracer::CosineWeightedDiffuseReflection(vec3 _normal)
{
    // Construct the normal vector
    float r0 = Xor32RNG(seed), r1 = Xor32RNG(seed);
    float r2 = glm::sqrt(r0);
    float theta = 2 * PI * r1;
    float x = r2 * cosf(theta);
    float y = r2 * sinf(theta);
    vec3 r = vec3(x, y, glm::sqrt(1.0f - r0));

    // Make a random vector ensuring it is not equal to the normal
    vec3 w = vec3(1, 0, 0);
    if (abs(_normal.x) >= 0.99f)
        w = vec3(0, 1, 0);

    // Construct normalized tangent and bitangent vectors
    vec3 t = CROSS(_normal, w);
    NORMALIZE(t);
    vec3 b = CROSS(t, _normal);

    // Tranform the input normal to local space
    vec3 result;
    result = r.x * t + r.y * b + r.z * _normal;

    return result;
}

float Raytracer::Xor32RNG(uint& _seed)
{
    _seed ^= _seed << 13;
    _seed ^= _seed >> 17;
    _seed ^= _seed << 5;
    return _seed * 2.3283064365387e-10f;
}

#pragma endregion




