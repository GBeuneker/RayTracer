#include "template.h"
#include "bvhNode.h"
#include "scene.h"

// The index of the left node we're currently checking
int left_index = 0;

// Subdivide this BVHNode into smaller BVHNodes
void BVHNode::SubDivide()
{
    // For less than 3 primitives, don't subdivide
    if (this->count < 3)
        return;

    // Cache the current pool index
    int left_cached = poolIndex;
    left_index = poolIndex;

    // Move pool index 2 places to the next child node
    poolIndex += 2;

    // Determine the best split position, axis and if we should divide further.
    float splitPosition; vec3 checkAxis; bool shouldDivide;
    DetermineSplitPlane(splitPosition, checkAxis, shouldDivide);

    if (shouldDivide)
    {
        Partition(splitPosition, checkAxis);

        left_index = left_cached;
        bvhPool[left_index]->SubDivide();

        left_index = left_cached;
        bvhPool[left_index + 1]->SubDivide();

        this->leftFirst = left_cached;
        this->count = 0;
    }
}

// Determine the best split plane for all the primitives in this node.
void BVHNode::DetermineSplitPlane(float &splitPosition_out, vec3 &checkAxis_out, bool &shouldDivide_out)
{
    float minX, minY, minZ;
    float bestHeuristic = FLT_MAX;
    vec3 xAxis = vec3(1, 0, 0), yAxis = vec3(0, 1, 0), zAxis = vec3(0, 0, 1);

    // Determine best possible split position and splitaxis.
    for (uint i = leftFirst; i < leftFirst + count; i++)
    {
        vec3 centroid = primitives[primIndices[i]]->GetCentroid();

        // Determine heuristic for split planes across all axes
        minX = CalculateHeuristic(centroid.x, xAxis);
        minY = CalculateHeuristic(centroid.y, yAxis);
        minZ = CalculateHeuristic(centroid.z, zAxis);

        // Check if heuristic is better and update the values
        if (minX < bestHeuristic)
        {
            bestHeuristic = minX;
            splitPosition_out = centroid.x;
            checkAxis_out = xAxis;
        }
        if (minY < bestHeuristic)
        {
            bestHeuristic = minY;
            splitPosition_out = centroid.y;
            checkAxis_out = yAxis;
        }
        if (minZ < bestHeuristic)
        {
            bestHeuristic = minZ;
            splitPosition_out = centroid.z;
            checkAxis_out = zAxis;
        }
    }

    // Determine if we should divide further.
    shouldDivide_out = bestHeuristic < bounds.GetSurfaceArea() * count;
}

// Partition this node's primitives into left and right
void BVHNode::Partition(float _splitPosition, vec3 _checkAxis)
{
    int i = this->leftFirst;
    int j = i + this->count - 1;

    while (i <= j)
    {
        // Primitive is left of the split plane
        if (DOT(primitives[primIndices[i]]->GetCentroid(), _checkAxis) < _splitPosition)
        {
            bvhPool[left_index]->count++;
            i++;
        }
        // Primitive is right of the split plane
        else
        {
            swap(primIndices[i], primIndices[j]);
            j--;
        }
    }

    bvhPool[left_index]->leftFirst = this->leftFirst;

    bvhPool[left_index + 1]->count = this->count - bvhPool[left_index]->count;
    bvhPool[left_index + 1]->leftFirst = bvhPool[left_index]->leftFirst + bvhPool[left_index]->count;

    bvhPool[left_index]->bounds = CalculateBounds(bvhPool[left_index]);
    bvhPool[left_index + 1]->bounds = CalculateBounds(bvhPool[left_index + 1]);
}

#pragma region Calculations

void BVHNode::Traverse(Ray &_ray, int &primIndex_out, bool isShadowRay)
{
    if (!bounds.Intersection(_ray))
        return;

    traversals++;

    if (IsLeaf())
        NearestIntersection(_ray, primIndex_out, isShadowRay);
    else
    {
        bvhPool[leftFirst]->Traverse(_ray, primIndex_out, isShadowRay);
        bvhPool[leftFirst + 1]->Traverse(_ray, primIndex_out, isShadowRay);
    }
}

void BVHNode::NearestIntersection(Ray &_ray, int &primIndex_out, bool isShadowRay)
{
    int closestPrimIndex = -1;
    float nearestIntersection = _ray.GetDistance();

    // Check all the primitives in this node.
    for (size_t i = leftFirst; i < leftFirst + count; i++)
    {
        // The primitive index we're currently checking
        int currentPrimIndex = primIndices[i];

        // Get the closest intersection distance with the primitive.
        float intersectionDist = primitives[currentPrimIndex]->GetIntersectionDist(_ray);

        // We have found a valid intersection which is nearer than our current nearest intersection
        if (intersectionDist >= 0 && intersectionDist < nearestIntersection)
        {
            // Save the new intersection distance
            nearestIntersection = intersectionDist;
            // Save the index of the primitive intersected
            closestPrimIndex = currentPrimIndex;

            if (isShadowRay) //early out when shadowray. hitting only 1 object is enough
                break;
        }
    }

    if (closestPrimIndex >= 0)
    {
        // Set the index of the primitive
        primIndex_out = closestPrimIndex;
        // Set the intersection distance and get the intersection point
        _ray.SetDistance(nearestIntersection);
        vec3 intersectionPoint = _ray.GetPosition();
        // Calculate normal
        vec3 n = primitives[closestPrimIndex]->GetNormal(intersectionPoint);
        NORMALIZE(n);
        _ray.SetNormal(n);
    }
}

// Calculate the heuristic based on surface area.
float BVHNode::CalculateHeuristic(float _splitPosition, vec3 _checkAxis)
{
    // Setup variables
    vec3 minL = vec3(FLT_MAX, FLT_MAX, FLT_MAX);
    vec3 maxL = vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
    vec3 minR = vec3(FLT_MAX, FLT_MAX, FLT_MAX);
    vec3 maxR = vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
    int sizeL = 0;
    int sizeR = 0;

    // loop over all the primitives in this node.
    for (size_t i = leftFirst; i < leftFirst + count; i++)
    {
        Primitive* prim = primitives[primIndices[i]];
        // Primitive is left of our splitplane
        if (DOT(prim->GetCentroid(), _checkAxis) < _splitPosition)
        {
            // Adjust the size of our bounding box
            minL.x = MIN(minL.x, prim->GetMinX());
            minL.y = MIN(minL.y, prim->GetMinY());
            minL.z = MIN(minL.z, prim->GetMinZ());
            maxL.x = MAX(maxL.x, prim->GetMaxX());
            maxL.y = MAX(maxL.y, prim->GetMaxY());
            maxL.z = MAX(maxL.z, prim->GetMaxZ());
            // Add primitive to the left side
            sizeL++;
        }
        // Primitive is right of our splitplane
        else
        {
            // Adjust the size of our bounding box
            minR.x = MIN(minR.x, prim->GetMinX());
            minR.y = MIN(minR.y, prim->GetMinY());
            minR.z = MIN(minR.z, prim->GetMinZ());
            maxR.x = MAX(maxR.x, prim->GetMaxX());
            maxR.y = MAX(maxR.y, prim->GetMaxY());
            maxR.z = MAX(maxR.z, prim->GetMaxZ());
            // Add primitive to the right side
            sizeR++;
        }
    }

    // Determine size of left bounding box
    float xLengthL = maxL.x - minL.x;
    float yLengthL = maxL.y - minL.y;
    float zLengthL = maxL.z - minL.z;
    // Determine size of right bounding box
    float xLengthR = maxR.x - minR.x;
    float yLengthR = maxR.y - minR.y;
    float zLengthR = maxR.z - minR.z;

    // Calculate the surface area of the left box
    float surfaceAreaL = 0;
    if (sizeL > 0)
        surfaceAreaL = xLengthL * yLengthL * 2 + xLengthL * zLengthL * 2 + yLengthL * zLengthL * 2;

    // Calculate the surface area of the right box
    float surfaceAreaR = 0;
    if (sizeR > 0)
        surfaceAreaR = xLengthR * yLengthR * 2 + xLengthR * zLengthR * 2 + yLengthR * zLengthR * 2;

    // Calculate heuristic based on surface area and amount of objects inside it
    return surfaceAreaL * sizeL + surfaceAreaR * sizeR;
}

#pragma endregion
