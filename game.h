#pragma once

#define SCRWIDTH	 1280
#define SCRHEIGHT	 720
#define TRACEMETHOD  2  //1 for whitted, 2 for bvh

namespace Tmpl8 {
    class Surface;
    class Game
    {
    public:
        void SetTarget(Surface* _Surface) { surface = _Surface; }
        void Init();
        void Shutdown() { };
        void HandleInput(float dt);
        void Tick(float dt);
        void KeyUp(int a_Key);
        void KeyDown(int a_Key);
        void UpdateScreenInfo();
        void PathTrace();
    private:
        Surface * surface;
    };

};