// Template, major revision 6
// IGAD/NHTV - Jacco Bikker - 2006-2015

#pragma once

#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE

#include "math.h"
#include "stdlib.h"
#include "emmintrin.h"
#include "stdio.h"
#include "windows.h"
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include "surface.h"
#include "game.h"
#include <vector>
#include "freeimage.h"
#include "threads.h"

extern "C"
{
#include "glew.h" 
}
#include "gl.h"
#include "io.h"
#include <ios>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "SDL.h"
#include "SDL_syswm.h"
#include "wglext.h"
#include "fcntl.h"

using namespace Tmpl8;				// to use template classes
using namespace glm;				// to use glm vector stuff
using namespace std;				// to use stl vectors

inline float Rand(float range) { return ((float)rand() / RAND_MAX) * range; }
inline int IRand(int range) { return rand() % range; }
int filesize(FILE* f);
#define MALLOC64(x) _aligned_malloc(x,64)
#define FREE64(x) _aligned_free(x)

namespace Tmpl8 {

#define MIN(a,b) (((a)>(b))?(b):(a))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define DOT(a,b) (a.x*b.x + a.y*b.y + a.z*b.z)
#define CROSS(a,b) vec3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x)
#define NORMALIZE(a)	{float l=1/sqrtf(a.x*a.x+a.y*a.y+a.z*a.z);a.x*=l;a.y*=l;a.z*=l;}
#define LENGTH(a)		(sqrtf(a.x*a.x+a.y*a.y+a.z*a.z))
#define SQRLENGTH(a)	(a.x*a.x+a.y*a.y+a.z*a.z)
#define SQRDISTANCE(a,b) ((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z))


#define PI					3.14159265358979323846264338327950288419716939937510582097494459072381640628620899862803482534211706798f
#define INVPI				1.0f / PI
#define EPSILON				0.00001f
#define WHITTEDTRACE		1
#define BVHTRACE			2

#define PREFETCH(x)			_mm_prefetch((const char*)(x),_MM_HINT_T0)
#define PREFETCH_ONCE(x)	_mm_prefetch((const char*)(x),_MM_HINT_NTA)
#define PREFETCH_WRITE(x)	_m_prefetchw((const char*)(x))
#define loadss(mem)			_mm_load_ss((const float*const)(mem))
#define broadcastps(ps)		_mm_shuffle_ps((ps),(ps), 0)
#define broadcastss(ss)		broadcastps(loadss((ss)))

    extern uint seed;

    struct timer
    {
        typedef long long value_type;
        static double inv_freq;
        value_type start;
        timer() : start(get()) { init(); }
        float elapsed() const { return (float)((get() - start) * inv_freq); }
        static value_type get()
        {
            LARGE_INTEGER c;
            QueryPerformanceCounter(&c);
            return c.QuadPart;
        }
        static double to_time(const value_type vt) { return double(vt) * inv_freq; }
        void reset() { start = get(); }
        static void init()
        {
            LARGE_INTEGER f;
            QueryPerformanceFrequency(&f);
            inv_freq = 1000. / double(f.QuadPart);
        }
    };

    class Color
    {
    public:
        Color() : r(0), g(0), b(0) {};
        Color(float a_r, float a_g, float a_b) : r(a_r), g(a_g), b(a_b) {};
        Color(int _hexValue) : r(((_hexValue >> 16) & 0xFF) / 255.0), g(((_hexValue >> 8) & 0xFF) / 255.0), b((_hexValue & 0xFF) / 255.0) {};
        void operator += (Color& a_V) { r += a_V.r; g += a_V.g; b += a_V.b; }
        void operator += (Color* a_V) { r += a_V->r; g += a_V->g; b += a_V->b; }
        Color operator + (Color& _c) { return Color(r + _c.r, g + _c.g, b + _c.b); }
        Color operator + (Color* _c) { return Color(r + _c->r, g + _c->g, b + _c->b); }
        void operator -= (Color& a_V) { r -= a_V.r; g -= a_V.g; b -= a_V.b; }
        void operator -= (Color* a_V) { r -= a_V->r; g -= a_V->g; b -= a_V->b; }
        Color operator - (Color& _c) { return Color(r - _c.r, g - _c.g, b - _c.b); }
        Color operator - (Color* _c) { return Color(r - _c->r, g - _c->g, b - _c->b); }
        void operator *= (float f) { r *= f; g *= f; b *= f; }
        void operator /= (float f) { r /= f; g /= f; b /= f; }
        Color operator / (Color& _c) { return Color(r / _c.r, g / _c.g, b / _c.b); }
        Color operator / (Color* _c) { return Color(r / _c->r, g / _c->g, b / _c->b); }
        Color operator / (float f) { return Color(r / f, g / f, b / f); }
        void operator *= (Color& a_V) { r *= a_V.r; g *= a_V.g; b *= a_V.b; }
        void operator *= (Color* a_V) { r *= a_V->r; g *= a_V->g; b *= a_V->b; }
        Color operator * (Color& _c) { return Color(r*_c.r, g*_c.g, b*_c.b); }
        Color operator * (Color* _c) { return Color(r*_c->r, g*_c->g, b*_c->b); }
        Color operator * (float f) { return Color(r*f, g*f, b*f); }
        bool operator == (Color& a_V) { return r == a_V.r && g == a_V.g && b == a_V.b; }
        bool operator == (Color* a_V) { return r == a_V->r && g == a_V->g && b == a_V->b; }
        bool operator != (Color& a_V) { return r != a_V.r || g != a_V.g || b != a_V.b; }
        bool operator != (Color* a_V) { return r != a_V->r || g != a_V->g || b != a_V->b; }
        union {
            struct { float r, g, b; };
        };
        unsigned long GetHexcode()
        {
            byte _r = (byte)clamp((r * 255), 0.0f, 255.0f);
            byte _g = (byte)clamp((g * 255), 0.0f, 255.0f);
            byte _b = (byte)clamp((b * 255), 0.0f, 255.0f);
            return ((_r & 0xff) << 16) + ((_g & 0xff) << 8) + (_b & 0xff);
        }

    };

    typedef unsigned char uchar;
    typedef unsigned char byte;

#define BADFLOAT(x) ((*(uint*)&x & 0x7f000000) == 0x7f000000)

}; // namespace Tmpl8