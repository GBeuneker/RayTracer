#include "template.h"
#include "camera.h"

timer time;

Camera::Camera(vec3 _position, vec3 _dir, float _fov, Screen _screen, Surface* _surface)
{
	position = _position;
	direction = _dir;
	fov = _fov;
	surface = _surface;

	float aR = _screen.GetWidth() / _screen.GetHeight();

	// Calculate the screen parameters based on the camera position.
	vec3 screenCenter = position + fov * direction;
	vec3 p0 = screenCenter + vec3(-1 * aR, -1, 0);
	vec3 p1 = screenCenter + vec3(1 * aR, -1, 0);
	vec3 p2 = screenCenter + vec3(-1 * aR, 1, 0);

	// Pass the calculated parameters to the screen.
	_screen.Initialize(screenCenter, p0, p1, p2);
	// Attach the screen to the camera.
	screen = _screen;
}

void Camera::Move(vec3 _mov)
{
	position += direction * _mov.z;
	position += vec3(1, 0, 0) * _mov.x;

	UpdateScreen();
}

void Camera::Rotate(float xRot, float yRot)
{
	float theta = -xRot * (PI / 180); //get degrees
	mat3x3 rotMat1 = mat3x3(cos(theta), 0, sin(theta), 0, 1, 0, -1 * sin(theta), 0, cos(theta));

	theta = yRot * (PI / 180); //get degrees
	mat3x3 rotMat2 = mat3x3(1, 0, 0, 0, cos(theta), -1 * sin(theta), 0, sin(theta), cos(theta));

	vec3 newDirection = GetDirection() * rotMat1 * rotMat2;

	direction = newDirection;

	UpdateScreen();
}

void Camera::UpdateScreen()
{
	vec3 screenCenter = position + fov * direction;
	vec3 p0 = screenCenter + vec3(-1 * screen.GetRatio(), -1, 0);
	vec3 p1 = screenCenter + vec3(1 * screen.GetRatio(), -1, 0);
	vec3 p2 = screenCenter + vec3(-1 * screen.GetRatio(), 1, 0);

	screen.Initialize(screenCenter, p0, p1, p2);
}
