#include "template.h"
#include "scene.h"
#include "modelLoader.h"

vector<Primitive*> primitives;
uint* primIndices;
ModelLoader * modelLoader;

Scene::Scene()
{

}


Scene::~Scene()
{
}

void Scene::Initialize()
{
    BuildPrims();
    if (TRACEMETHOD == BVHTRACE)
        BuildBVH();
}

void Scene::BuildPrims()
{
#pragma region Models
#if 0
    modelLoader->LoadModel("bunny", this, vec3(0, 0, 50), 3);
#endif 
#pragma endregion

#pragma region Primitives
#if 1
    // -----------------------------------------------------------
    // Spheres
    // -----------------------------------------------------------
    // small sphere
    Sphere* s1 = new Sphere(vec3(-3, 2, 40), 1);
    s1->SetName("blue sphere");
    s1->GetMaterial()->SetDiffuse(0.5);
    s1->GetMaterial()->SetColor(Color(0.9, 0, 0.9));
    // small sphere 2
    Sphere* s2 = new Sphere(vec3(0, 2, 40), 1);
    s2->SetName("reflective sphere");
    s2->GetMaterial()->SetDiffuse(0);
    s2->GetMaterial()->SetColor(Color(1, 1, 1));
    // small sphere 3
    Sphere* s3 = new Sphere(vec3(3, 2, 40), 1.0f);
    s3->SetName("red sphere");
    s3->GetMaterial()->SetDiffuse(1);
    s3->GetMaterial()->SetColor(Color(0.9, 0, 0));
    // sphere pedestal
    Sphere* s4 = new Sphere(vec3(3, 4, 40), 1.0f);
    s4->SetName("sphere pedestal");
    s4->GetMaterial()->SetDiffuse(1);
    s4->GetMaterial()->SetColor(Color(0.9, 0.9, 0.9));
    // sphere pedestal
    Sphere* s5 = new Sphere(vec3(0, 4, 40), 1.0f);
    s5->SetName("sphere pedestal");
    s5->GetMaterial()->SetDiffuse(1);
    s5->GetMaterial()->SetColor(Color(0.9, 0.9, 0.9));
    // sphere pedestal
    Sphere* s6 = new Sphere(vec3(-3, 4, 40), 1.0f);
    s6->SetName("sphere pedestal");
    s6->GetMaterial()->SetDiffuse(1);
    s6->GetMaterial()->SetColor(Color(0.9, 0.9, 0.9));

    primitives.push_back(s1);
    primitives.push_back(s2);
    primitives.push_back(s3);
    primitives.push_back(s4);
    primitives.push_back(s5);
    primitives.push_back(s6);
#endif // Primitives

#pragma endregion

#pragma region Walls
#if 1
    // -----------------------------------------------------------
    // Walls
    // -----------------------------------------------------------
    Square* sq1 = new Square(vec3(6, 0, 40), vec3(-1, 0, 0), vec3(0, 1, 0), vec2(50, 50));
    sq1->SetName("Right Wall");
    sq1->GetMaterial()->SetDiffuse(1);
    sq1->GetMaterial()->SetColor(Color(0.8, 0.1, 0.1));

    Square* sq2 = new Square(vec3(-6, 0, 40), vec3(1, 0, 0), vec3(0, 1, 0), vec2(50, 50));
    sq2->SetName("Left Wall");
    sq2->GetMaterial()->SetDiffuse(1);
    sq2->GetMaterial()->SetColor(Color(0.1, 0.1, 0.8));

    Square* sq3 = new Square(vec3(0, -4, 40), vec3(0, 1, 0), vec3(1, 0, 0), vec2(50, 50));
    sq3->SetName("Top Wall");
    sq3->GetMaterial()->SetDiffuse(1);
    sq3->GetMaterial()->SetColor(Color(1, 1, 1));

    Square* sq4 = new Square(vec3(0, 4, 40), vec3(0, -1, 0), vec3(-1, 0, 0), vec2(50, 50));
    sq4->SetName("Bottom Wall");
    sq4->GetMaterial()->SetDiffuse(1);
    sq4->GetMaterial()->SetColor(Color(1, 1, 1));

    Square* sq5 = new Square(vec3(0, 0, 50), vec3(0, 0, -1), vec3(0, 1, 0), vec2(10, 10));
    sq5->SetName("Back Wall");
    sq5->GetMaterial()->SetDiffuse(0.5);
    sq5->GetMaterial()->SetColor(Color(0.7, 0.9, 0.7));

    Square* sq6 = new Square(vec3(0, 0, 10), vec3(0, 0, 1), vec3(0, 1, 0), vec2(10, 10));
    sq6->SetName("Front Wall");
    sq6->GetMaterial()->SetDiffuse(0.5);
    sq6->GetMaterial()->SetColor(Color(1, 1, 1));

    primitives.push_back(sq1);
    primitives.push_back(sq2);
    primitives.push_back(sq3);
    primitives.push_back(sq4);
    primitives.push_back(sq5);
    primitives.push_back(sq6);
#endif
#pragma endregion

#pragma region Lights

    // -----------------------------------------------------------
    // Lights
    // -----------------------------------------------------------
    AreaLight* l1 = new AreaLight(vec3(0, -3.99, 38), vec3(0, 1, 0), vec3(1, 0, 0), vec2(3, 3), 3.0f, Color(1, 1, 1));
    l1->SetName("Area Light 1");
    lights.push_back(l1);
    primitives.push_back(l1);

#pragma endregion
}

void Scene::AddModel(vector<Triangle*> _model)
{
    for (int i = 0; i < _model.size(); i++)
    {
        primitives.push_back(_model[i]);
    }
}

void Scene::BuildBVH()
{
    bvhTree = new BVH();
    bvhTree->ConstructBVH();
}

Light* Scene::GetRandomLight()
{
    int wut = rand();
    int randomIndex = rand() % lights.size();
    return lights[randomIndex];
}
