#include "template.h"
#include "ray.h"

Ray::Ray(vec3 _origin, vec3 _dir)
{
    Reset();
    SetOrigin(_origin);
    SetDirection(_dir);
}

Ray::~Ray()
{
}