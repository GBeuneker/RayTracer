#pragma once
#include "template.h"

struct Light
{
	vec3 color;
	vec3 position;
};