#pragma once
#include "template.h"
#include "ray.h"
#include "material.h"

///--------------------------------------------------
// Base primitive class
///--------------------------------------------------
class Primitive
{
public:
    enum {
        SPHERE = 1,
        PLANE,
        TRIANGLE,
        SQUARE,
        LIGHT
    };
    Material* GetMaterial() { return &material; }
    void SetMaterial(Material& _material) { material = _material; }
    virtual int GetType() = 0;
    virtual float GetIntersectionDist(Ray _ray) = 0;
    virtual vec3 GetNormal(vec3& _position) = 0;
    void SetName(string _name) { name = _name; }
    string GetName() { return name; }
    float GetMaxX() { return maxX; }
    float GetMinX() { return minX; }
    float GetMaxY() { return maxY; }
    float GetMinY() { return minY; }
    float GetMaxZ() { return maxZ; }
    float GetMinZ() { return minZ; }
    vec3 GetCentroid() { return centroid; }
protected:
    string name;
    Material material;
    float maxX = 0;
    float minX = 0;
    float maxY = 0;
    float minY = 0;
    float maxZ = 0;
    float minZ = 0;
    vec3 centroid = vec3(0, 0, 0);
};

/*======================================== SHAPES ========================================*/

///--------------------------------------------------
// Sphere Primitive
///--------------------------------------------------
class Sphere : public Primitive
{
public:
    Sphere(vec3 _center, float _radius);
    int GetType() { return SPHERE; }
    int Intersect(Ray& _ray, float& _dist);
    vec3 GetNormal(vec3& _position);
    vec3 GetCenter() { return center; }
    float GetRadius() { return radius; }
    float GetIntersectionDist(Ray ray);

private:
    vec3 center;
    float radius;
};

///--------------------------------------------------
// Plane Primitive
///--------------------------------------------------
class Plane : public Primitive
{
public:
    Plane(vec3 _normal, float _distance);
    int GetType() { return PLANE; }
    float GetIntersectionDist(Ray _ray);
    vec3 GetNormal(vec3& _position) { return normal; }
private:
    vec3 normal;
    float distance;
};

///--------------------------------------------------
// Triangle Primitive
///--------------------------------------------------
class Triangle : public Primitive
{
public:
    Triangle(vec3 _p0, vec3 _p1, vec3 _p2);
    int GetType() { return TRIANGLE; }
    float GetIntersectionDist(Ray _ray);
    vec3 GetNormal(vec3& _position) { return normal; }
private:
    vec3 normal;
    vec3 p0, p1, p2;
    vec3 edge1, edge2;
};

///--------------------------------------------------
// Square Primitive
///--------------------------------------------------
class Square : public Primitive
{
public:
    Square(vec3 _position, vec3 _normal, vec3 _right, vec2 _size);
    int GetType() { return SQUARE; }
    float GetIntersectionDist(Ray _ray);
    vec3 GetNormal(vec3& _position) { return normal; }
private:
    vec3 center, normal;
    vec2 size;
    vec3 u, v;
    Triangle* t1, *t2;
};

/*======================================== LIGHTS ========================================*/

///--------------------------------------------------
// Light Base Class Primitive
///--------------------------------------------------
class Light : public Primitive
{
public:
    Light(float _intensity, Color _color);
    virtual vec3 GetRandomPoint() = 0;
    virtual float GetArea() = 0;
    Color GetColor() { return color; }
    virtual float GetIntensity() { return intensity; }
    virtual vec3 GetCenter() = 0;
protected:
    float intensity;
    Color color;
};

///--------------------------------------------------
// Point Light Primitive
///--------------------------------------------------
class PointLight : public Light
{
public:
    PointLight(vec3 _center, float _intensity, Color _color);
    int GetType() { return LIGHT; }
    float GetIntersectionDist(Ray _ray) { return 0; }
    vec3 GetNormal(vec3& _position) { return vec3(0, 0, 0); }
    vec3 GetCenter() { return center; }
    float GetIntensity() { return intensity; }
    Color GetColor() { return color; }
    vec3 GetRandomPoint() { return center; }
    float GetArea() { return 0; }
private:
    vec3 center;
};

///--------------------------------------------------
// Area Light Primitive
///--------------------------------------------------
class AreaLight : public Light
{
public:
    AreaLight(vec3 _position, vec3 _normal, vec3 _right, vec2 _size, float _intensity, Color _color);
    int GetType() { return LIGHT; }
    float GetIntersectionDist(Ray _ray);
    vec3 GetNormal(vec3& _position) { return normal; }
    vec3 GetCenter() { return center; }
    float GetIntensity() { return intensity; }
    Color GetColor() { return color; }
    vec3 GetRandomPoint();
    float GetArea();
private:
    float distance;
    vec3 center;
    vec3 normal;
    vec2 size;
    vec3 u, v;
    Triangle* t1, *t2;
};

