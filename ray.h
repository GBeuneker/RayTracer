#pragma once
#include "template.h"

class Ray
{
public:
    enum {
        NOHIT = 0,
        HIT = 1,
    };
    Ray(vec3 _origin, vec3 _direction); //constructor  Position(ray) = Origin + t * dir
    Ray() {};

    vec3 GetOrigin() { return origin; }
    void SetOrigin(vec3 _origin) { origin = _origin; updateHitPosition(); }
    vec3 GetDirection() { return direction; }
    void SetDirection(vec3 _direction) { direction = _direction;  updateHitPosition(); }
    vec3 GetNormal() { return hitNormal; }
    void SetNormal(vec3 _normal) { hitNormal = _normal; NORMALIZE(hitNormal); }
    vec3 GetPosition() { return hitPosition; }
    float GetDistance() { return t; }
    void SetDistance(float _t) { t = _t;  updateHitPosition(); }
    int GetHitState() { return hitState; }
    void SetHitState(int _state) { hitState = _state; }
    void Reset()
    {
        t = FLT_MAX;
        hitNormal = vec3(0, 0, 0);
        hitState = NOHIT;
    }
    ~Ray(); //destructor

private:
    void updateHitPosition() { hitPosition = origin + direction * t; }
    vec3 origin, direction;
    vec3 hitNormal, hitPosition;
    float t;
    int hitState;
};