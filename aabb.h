#pragma once
#include "ray.h"


class AABB
{
public:
	AABB();
	AABB(vec3 _min, vec3 _max);
	vec3 GetMin() { return min; }
	vec3 GetMax() { return max; }
	float GetSurfaceArea();
	bool Intersection(Ray _ray);

private:
	vec3 min; //minimal coordinates
	vec3 max; //maximal coordinates
};

