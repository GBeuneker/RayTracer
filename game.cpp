#include "template.h"
#include "camera.h"
#include "primitives.h"
#include "ray.h"
#include "scene.h"
#include "raytracer.h"
#include <string>

Camera* camera;
Raytracer* rayTracer;
// Set to true if you would like to render the traversal steps on screen.
bool drawTraversals = false;
string camInfo, camInfo2, camInfo3;
int steps = 0;
Color* pixels;

// -----------------------------------------------------------
// Initialize the game
// -----------------------------------------------------------
void Game::Init()
{
    // Screen initialization
    Screen camScreen = Screen(SCRWIDTH, SCRHEIGHT);

    // Camera initialization
    camera = new Camera(vec3(0, 0, 12), vec3(0, 0, 1), 5.0f, camScreen, surface);

    // Scene initialization
    Scene* scene = new Scene();
    scene->Initialize();

    rayTracer = new Raytracer(scene);

    pixels = new Color[SCRWIDTH + surface->GetPitch()*SCRHEIGHT];
    for (int i = 0; i < SCRWIDTH + surface->GetPitch()*SCRHEIGHT; ++i)
    {
        pixels[i] = Color(0, 0, 0);
    }
}

// -----------------------------------------------------------
// Input handling
// -----------------------------------------------------------
void Game::HandleInput(float dt)
{

}

// -----------------------------------------------------------
// Main game tick function
// -----------------------------------------------------------
void Game::Tick(float dt)
{
    PathTrace();
    //UpdateScreenInfo();
}

void Game::PathTrace()
{
    steps++;
    seed = (steps * 121818121) * 782070287;
    for (int i = 0; i < (int)camera->GetScreen().GetWidth(); i++)
    {
#pragma omp parallel for
        for (int j = 0; j < (int)camera->GetScreen().GetHeight(); j++)
        {
            float u = i / camera->GetScreen().GetWidth();
            float v = j / camera->GetScreen().GetHeight();

            vec3 p0 = camera->GetScreen().GetP0();
            vec3 p1 = camera->GetScreen().GetP1();
            vec3 p2 = camera->GetScreen().GetP2();
            vec3 screenPoint = p0 + u * (p1 - p0) + v * (p2 - p0);

            vec3 direction = screenPoint - camera->GetPosition();
            NORMALIZE(direction);
            Ray r = Ray(camera->GetPosition(), direction);

            Primitive* primHit = rayTracer->Intersect(r);
            Material *mat = primHit->GetMaterial();
            Color c_new = Color(0, 0, 0);
            if (r.GetHitState() == Ray::HIT)
                c_new = mat->GetColor() * (rayTracer->SampleDirect(r, primHit) + rayTracer->SampleIndirect(r, primHit, false));
            if (drawTraversals)
            {
                // Plot the traversal steps on the screen.
                c_new += Color(0.1f * traversals, 0.1f * traversals, 0);
                traversals = 0;
            }

            pixels[i + surface->GetPitch()*j] += c_new;

            Color c = pixels[i + surface->GetPitch()*j];
            c /= (float)steps;

            surface->Plot(i, j, c.GetHexcode());
        }
    }
    if (steps % 10 == 0)
        printf("tenfold of steps done \n");
}

void Game::KeyDown(int a_Key)
{
    steps = 0;
    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(VK_UP))
    {
        printf("key up has been pressed \n");
        camera->Rotate(0, 1);
    }

    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(VK_DOWN))
    {
        printf("key down has been pressed \n");
        camera->Rotate(0, -1);
    }

    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(VK_LEFT))
    {
        printf("key left has been pressed \n");
        camera->Rotate(1, 0);
    }

    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(VK_RIGHT))
    {
        printf("key right has been pressed \n");
        camera->Rotate(-1, 0);
    }

    // W
    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(0x57))
    {
        printf("left shift has been pressed \n");
        camera->Move(vec3(0, 0, 1));
    }

    // A
    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(0x41))
    {
        printf("left shift has been pressed \n");
        camera->Move(vec3(-1, 0, 0));
    }

    // S
    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(0x53))
    {
        printf("left shift has been pressed \n");
        camera->Move(vec3(0, 0, -1));
    }

    // D
    if (GetAsyncKeyState(a_Key) != GetAsyncKeyState(0x44))
    {
        printf("left shift has been pressed \n");
        camera->Move(vec3(1, 0, 0));
    }

    //UpdateScreenInfo();
}

void Game::KeyUp(int a_Key)
{
}

void Game::UpdateScreenInfo()
{
    camInfo = "Camera position: x=";
    camInfo += to_string(camera->GetPosition().x);
    camInfo += " y=";
    camInfo += to_string(camera->GetPosition().y);
    camInfo += " z=";
    camInfo += to_string(camera->GetPosition().z);
    surface->Print(_strdup(camInfo.c_str()), 10, 10, 0xffffff);

    camInfo2 = "Camera rotation: x=";
    camInfo2 += to_string(camera->GetDirection().x);
    camInfo2 += " y=";
    camInfo2 += to_string(camera->GetDirection().y);
    camInfo2 += " z=";
    camInfo2 += to_string(camera->GetDirection().z);
    surface->Print(_strdup(camInfo2.c_str()), 10, 30, 0xffffff);

    camInfo3 = "Camera FOV: ";
    camInfo3 += to_string(camera->GetFOV());
    surface->Print(_strdup(camInfo3.c_str()), 10, 50, 0xffffff);
}