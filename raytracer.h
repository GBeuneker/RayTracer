#pragma once
#include "template.h"
#include "surface.h"
#include "scene.h"
#include "camera.h"

class Raytracer
{
public:
    Raytracer(Scene* _scene);
    Color SampleDirect(Ray _ray, Primitive * _prim);
    Color SampleIndirect(Ray _ray, Primitive * _prim, bool lastSpecular);
    Primitive* Intersect(Ray &_ray);
    vec3 CosineWeightedDiffuseReflection(vec3 _normal);
    float Xor32RNG(uint & _seed);
    Ray Reflect(vec3 _rayDirection, vec3 _hitPosition, vec3 _normal);
    void NearestIntersection(Ray &_ray, int &primIndex);
    Ray shadowRay;
private:
    Scene * scene;
};

