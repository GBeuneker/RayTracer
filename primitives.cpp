#include "template.h"
#include "primitives.h"
#include "SDL_test_random.h"

// -----------------------------------------------------------
// Sphere functions
// -----------------------------------------------------------
Sphere::Sphere(vec3 _center, float _radius)
{
    center = _center;
    radius = _radius;
    maxX = center.x + radius;
    minX = center.x - radius;
    maxY = center.y + radius;
    minY = center.y - radius;
    maxZ = center.z + radius;
    minZ = center.z - radius;
    centroid = center;
}

int Sphere::Intersect(Ray & _ray, float & _dist)
{
    return 0;
}

vec3 Sphere::GetNormal(vec3 & _position)
{
    return (_position - center) * radius;
}

float Sphere::GetIntersectionDist(Ray ray)
{
    vec3 rayDir = ray.GetDirection();
    vec3 v = ray.GetOrigin() - this->GetCenter();
    float radius = this->GetRadius();

    float a = DOT(rayDir, rayDir);
    float b = DOT(2.0f * rayDir, v);
    float c = DOT(v, v) - radius * radius;

    float d = b * b - 4 * a*c;
    // There was no intersection
    if (d < 0)
        return -1;

    float rootD = sqrtf(d);

    // Return nearest intersection point
    return MIN(((-1 * b + rootD) / 2 * a), ((-1 * b - rootD) / 2 * a));
}

// -----------------------------------------------------------
// Light functions
// -----------------------------------------------------------
Light::Light(float _intensity, Color _color)
{
    intensity = _intensity;
    color = _color;

    color *= intensity;
    material.SetColor(color);
    material.SetLight(true);
    material.SetEmittence(color);
}


PointLight::PointLight(vec3 _center, float _intensity, Color _color)
    :Light(_intensity, _color)
{
    center = _center;
}

AreaLight::AreaLight(vec3 _position, vec3 _normal, vec3 _right, vec2 _size, float _intensity, Color _color)
    : Light(_intensity, _color)
{
    center = _position;
    normal = _normal;
    size = _size;

    u = _right;
    v = CROSS(u, normal);
    NORMALIZE(u);
    NORMALIZE(v);

    vec3 p0 = _position + (-u * size.x + v * size.y);
    vec3 p1 = _position + (u*size.x + v * size.y);
    vec3 p2 = _position + (u*size.x - v * size.y);
    vec3 p3 = _position + (-u * size.x - v * size.y);

    t1 = new Triangle(p0, p1, p2);
    t2 = new Triangle(p0, p2, p3);

    float epsilon = 0.01f;

    maxX = MAX(t1->GetMaxX(), t2->GetMaxX()) + epsilon;
    minX = MIN(t1->GetMinX(), t2->GetMinX()) - epsilon;
    maxY = MAX(t1->GetMaxY(), t2->GetMaxY()) + epsilon;
    minY = MIN(t1->GetMinY(), t2->GetMinY()) - epsilon;
    maxZ = MAX(t1->GetMaxZ(), t2->GetMaxZ()) + epsilon;
    minZ = MIN(t1->GetMinZ(), t2->GetMinZ()) - epsilon;
}

vec3 AreaLight::GetRandomPoint()
{
    vec3 answer = center;

    float xOffset = Rand(1) * (size.x * 2) - size.x;
    float yOffset = Rand(1) * (size.y * 2) - size.y;

    answer += v * xOffset;
    answer += u * yOffset;

    return answer;
}

float AreaLight::GetArea()
{
    return size.x * size.y * 4;
}

float AreaLight::GetIntersectionDist(Ray _ray)
{
    return MAX(t1->GetIntersectionDist(_ray), t2->GetIntersectionDist(_ray));
}

Square::Square(vec3 _position, vec3 _normal, vec3 _right, vec2 _size)
{
    center = _position;
    normal = _normal;
    size = _size;

    u = _right;
    v = CROSS(u, normal);
    NORMALIZE(u);
    NORMALIZE(v);

    vec3 p0 = _position + (-u * size.x + v * size.y);
    vec3 p1 = _position + (u*size.x + v * size.y);
    vec3 p2 = _position + (u*size.x - v * size.y);
    vec3 p3 = _position + (-u * size.x - v * size.y);

    t1 = new Triangle(p0, p1, p2);
    t2 = new Triangle(p0, p2, p3);

    float epsilon = 0.01f;

    maxX = MAX(t1->GetMaxX(), t2->GetMaxX()) + epsilon;
    minX = MIN(t1->GetMinX(), t2->GetMinX()) - epsilon;
    maxY = MAX(t1->GetMaxY(), t2->GetMaxY()) + epsilon;
    minY = MIN(t1->GetMinY(), t2->GetMinY()) - epsilon;
    maxZ = MAX(t1->GetMaxZ(), t2->GetMaxZ()) + epsilon;
    minZ = MIN(t1->GetMinZ(), t2->GetMinZ()) - epsilon;
}

float Square::GetIntersectionDist(Ray _ray)
{
    return MAX(t1->GetIntersectionDist(_ray), t2->GetIntersectionDist(_ray));
}


// -----------------------------------------------------------
// Plane functions
// -----------------------------------------------------------
Plane::Plane(vec3 _normal, float _distance)
{
    normal = _normal;
    distance = _distance;
    centroid = distance * normal;

    if (abs(normal.x) == 1)
    {
        maxX = normal.x * distance;
        minX = normal.x * distance;
    }
    if (abs(normal.y) == 1)
    {
        maxY = normal.y * distance;
        minY = normal.y * distance;
    }
    if (abs(normal.z) == 1)
    {
        maxZ = normal.z * distance;
        minZ = normal.z * distance;
    }
}

float Plane::GetIntersectionDist(Ray _ray)
{
    vec3 rayOr = _ray.GetOrigin();
    vec3 rayDir = _ray.GetDirection();

    return -((DOT(rayOr, normal) + distance) / (DOT(rayDir, normal)));
}

// -----------------------------------------------------------
// Triangle functions
// -----------------------------------------------------------
Triangle::Triangle(vec3 _p0, vec3 _p1, vec3 _p2)
{
    p0 = _p0;
    p1 = _p1;
    p2 = _p2;

    edge1 = p1 - p0;
    edge2 = p2 - p0;
    normal = CROSS(edge1, edge2);
    NORMALIZE(normal);

    maxX = MAX(p0.x, MAX(p1.x, p2.x));
    minX = MIN(p0.x, MIN(p1.x, p2.x));
    maxY = MAX(p0.y, MAX(p1.y, p2.y));
    minY = MIN(p0.y, MIN(p1.y, p2.y));
    maxZ = MAX(p0.z, MAX(p1.z, p2.z));
    minZ = MIN(p0.z, MIN(p1.z, p2.z));
    centroid = vec3((p0.x + p1.x + p2.x) / 3, (p0.y + p1.y + p2.y) / 3, (p0.z + p1.z + p2.z) / 3);
}

float Triangle::GetIntersectionDist(Ray _ray)
{
    vec3 p = CROSS(_ray.GetDirection(), edge2);
    float det = DOT(edge1, p);

    if (det > -EPSILON && det < EPSILON)
        return -1;
    float inv_det = 1 / det;

    vec3 t = _ray.GetOrigin() - p0;
    float u = inv_det * DOT(t, p);
    if (u < 0 || u > 1)
        return -1;

    vec3 q = CROSS(t, edge1);
    float v = inv_det * DOT(_ray.GetDirection(), q);
    if (v < 0 || u + v > 1)
        return -1;

    float dist = inv_det * DOT(edge2, q);

    if (dist > EPSILON)
        return dist;
    else
        return -1;
}
