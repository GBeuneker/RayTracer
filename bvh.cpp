#include "template.h"
#include "bvh.h"
#include "scene.h"

int traversals;
timer timeBVH;
BVHNode** bvhPool;
uint poolIndex;

BVH::BVH() {}

// Construct the entire Bounding Volume Hierarchy
void BVH::ConstructBVH()
{
    timeBVH.reset();

    // Make and fill the list of primitive indices
    primIndices = new uint[primitives.size()];
    for (uint i = 0; i < primitives.size(); i++)
        primIndices[i] = i;

    // Make and initialize the pool for all BVH Nodes
    bvhPool = new BVHNode*[2 * primitives.size() - 1];
    for (uint i = 0; i < 2 * primitives.size(); i++)
        bvhPool[i] = new BVHNode();

    // Assign root node.
    root = bvhPool[0];
    poolIndex = 2;

    // Initialize root node
    root->leftFirst = 0;
    root->count = primitives.size();
    root->bounds = CalculateBounds(root->leftFirst, root->count, primIndices);
    root->SubDivide();

    // Report time elapsed
    float elapsed = timeBVH.elapsed() / 1000;
    cout << "Time elapsed building BVH: " << elapsed << "s \n";
}

/// <summary> Calculates the Axis-Aligned bounding box for a list of primitives. </summary>
/// <param name="_first"> The index we start from. </param>
/// <param name="_count"> The amount of primives to consider. </param>
/// <param name="_primIndices"> The list of primitive indices we use.</param>
AABB CalculateBounds(int _first, int _count, uint* _primIndices)
{
    float minX = FLT_MAX;
    float maxX = -FLT_MAX;
    float minY = FLT_MAX;
    float maxY = -FLT_MAX;
    float minZ = FLT_MAX;
    float maxZ = -FLT_MAX;

    for (int i = _first; i < _first + _count; i++)
    {
        minX = MIN(minX, primitives[_primIndices[i]]->GetMinX());
        maxX = MAX(maxX, primitives[_primIndices[i]]->GetMaxX());
        minY = MIN(minY, primitives[_primIndices[i]]->GetMinY());
        maxY = MAX(maxY, primitives[_primIndices[i]]->GetMaxY());
        minZ = MIN(minZ, primitives[_primIndices[i]]->GetMinZ());
        maxZ = MAX(maxZ, primitives[_primIndices[i]]->GetMaxZ());
    }
    return AABB(vec3(minX, minY, minZ), vec3(maxX, maxY, maxZ));
}

/// <summary> Calculates the Axis-Aligned bounding box for a BVH Node. </summary>
/// <param name="_node"> The BVHNode we would like to calculate the bounding box for. </param>
AABB CalculateBounds(BVHNode* _node)
{
    return CalculateBounds(_node->leftFirst, _node->count, primIndices);
}
