#pragma once
class Screen
{
public:
	Screen(float _width = 0, float _height = 0);
	void Initialize(vec3 _center, vec3 _p0, vec3 _p1, vec3 _p2);
	float GetWidth() { return width; }
	float GetHeight() { return height; }
	float GetRatio() { return ratio; }
	vec3 GetCenter() { return center; }
	vec3 GetP0() { return p0; }
	vec3 GetP1() { return p1; }
	vec3 GetP2() { return p2; }
private:
	float width, height, ratio;
	vec3 center;
	vec3 p0, p1, p2;
};

