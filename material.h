#pragma once
class Material
{
public:
	Material();
	~Material();
	Color GetColor() { return color; }
	void SetColor(Color _color) { color = _color; }
	float GetDiffuse() { return diffuse; }
	void SetDiffuse(float _diffuse) { diffuse = clamp(_diffuse, 0.0f, 1.0f); reflection = 1.0f - diffuse; }
	float GetReflection() { return reflection; }
	Color GetEmittence() { return emittence; }
	void SetEmittence(Color _emittence) { emittence = _emittence; }
	bool IsLight() { return isLight; }
	void SetLight(bool _isLight) { isLight = _isLight; }
private:
	Color color;
	Color emittence = Color(0, 0, 0);
	float reflection = 0;
	float diffuse = 0;
	bool isLight = false;
};

