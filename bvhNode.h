#pragma once
#include "AABB.h"
#include "primitives.h"

class BVHNode
{
public:
    BVHNode() {};
    // The bounds of this node
    AABB bounds;
    // Index of left node or index of the first primitive to look from
    uint leftFirst = 0;
    // Amount of primitves under this node
    uint count = 0;

    void SubDivide();
    void Traverse(Ray &_ray, int & primIndex_out, bool isShadowRay);
    bool IsLeaf() { return count != 0; }
private:
    void DetermineSplitPlane(float &splitPosition_out, vec3 &checkAxis_out, bool &shouldDivide_out);
    void Partition(float _splitPosition, vec3 _checkAxis);
    void NearestIntersection(Ray &_ray, int & primIndex_out, bool isShadowRay);
    float CalculateHeuristic(float _splitPosition, vec3 _checkAxis);
};

